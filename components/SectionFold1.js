const SectionFold1 = () => {
    return(
         <section className="bg-light">
      <div className="container px-5">
        <div className="row gx-5 align-items-center justify-content-center justify-content-lg-between">
          <div className="col-12 col-lg-5">
            <h2 className="display-4 lh-1 mb-4">Designed with Simplicity in mind.</h2>
            <p className="lead fw-normal text-muted mb-5 mb-lg-0">
              Having complete control and understanding of your finances doesn't have to a difficult task.
              so we design the Rally app to handle records securely, and turn it to Informative insights with robust Analytics.
            </p>
          </div>
          <div className="col-sm-8 col-md-6">
            <div className="px-5 px-sm-0"><img className="img-fluid rounded-circle" src="https://source.unsplash.com/u8Jn2rzYIps/900x900"  /></div>
          </div>
        </div>
      </div>
    </section>
    )
}

export default SectionFold1